<?php

/**
 * @file
 * Adds independant logging for each site.
 *
 * Cribbed from http://webscope.co.nz/blog/sam/separate-apache-logs-your-aegir-sites
 * BIP logging configured by Tim McLaughlin
 */

/**
 * Implements hook_provision_apache_vhost_config().
 */
function logging_provision_apache_vhost_config($uri, $data) {
  return array(
    '',
    'LogFormat "%a %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\"" forwarded_combined',
    'ErrorLog  /var/log/apache2/' . $uri . '-error_log',
    'CustomLog /var/log/apache2/' . $uri . '-access_log forwarded_combined',
    '',
  );
}